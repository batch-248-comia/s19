console.log("Hello World");

//What are conditional statements?
//Conditional statements allow us to control the flow of our program
//It allows us to run a statement/instructions if a condition is met or run another separate instruction if otherwise

//if, else if, and else statement

let numA = -1;

//if statement
	//executes a statement if a specified condition is true

if(numA < 0){
	console.log("numA is less than 0");
}

/*
syntax

		if(consition){
			statement
		}
*/

// the result of the expression added in the if's condition must result to true, else, the statement inside the if() will not run

console.log(numA<0);//true

//example
numA = 0;

if (numA<0){
	console.log("Hello Again if numA is 0")
}
console.log(numA<0) 


//example

let city = "New York";

if (city === "New York"){
	console.log("Welcome to New York City!")
}

//not limited to numbers, but also to strings



//else if Clause

/* 
	-executes a statement if previous conditions are false and if the condition is true 
	- the "else if" clause is optional and can be added to capture additional conditions to change flow of a program

*/

//example
let numH = 1;

if(numA<0){
	console.log("If statement will run");
}else if(numH>0){
	console.log("Else statement will run");
}


//example
//if the if condition was passed and run, we will no longer evaluate the else if() and we end the process there

numA = 1;

if(numA>0){
	console.log("The number is greater than zero, the if statement runs");
}else if(numH>0){
	console.log("I am the else if statement.")
}

//another example

city = "Tokyo";

if (city === "New York"){
	console.log("Welcome to New York City!")
}else if(city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan!")
}

// else statement

	// else statement will execute if all other conditions are false
	//is optional and can be added to capture any other result to change the flow of a program


numA = -1;
numH = 1;

if(numA>0){
	console.log("Hello");
}else if(numH === 0){
	console.log("World");
}else{
	console.log("I will execute if all other conditions are false");
}

//example
// else statements should only be added if there is a preceeding if condition
//else statements by itself will not work, however, if statements will work even if there is

/*
else{
	console.log("will not run without an if");
}
*/

//another example

/* 
else if(numH === 0){
	console.log("hi, I'm else if");
}else{
	console.log("Hi, I'm else");
}
*/

//there should be a preceeding if() first




//if, else if, and else statement with functions

let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windspeed){

	if(windspeed < 30){
		return "Not a typhoon yet!";
	}
	else if(windspeed <= 61){
		return "Tropical depression detected";
	}
	else if(windspeed >=62 && windspeed <=88){
		return "Tropical storm detected.";
	}
	else if(windspeed >=89 || windspeed <=117){
		return	"Severe tropical storm detected.";
	}
	else {
		return	"Typhoon detected.";
	}
}

message = determineTyphoonIntensity(110);
console.log(message);

if(message == "Severe tropical storm detected."){
	console.warn(message);
}

//console.warn is a good way to print warnings in our console that could help us developers act on certain output within our code

/*Mini Activity 1

Create a function that can check whether a number is oddOrEvenChecker with one parameter

**A number must be provided as an argument
**Use the if and else statement
**It should not return anything
**There should be an alert if the condition is met
**Invoke and pass 1 argument to the oddOrEvenChecker function

*/

function oddOrEvenChecker(num){

	if(num % 2 === 0){
//		alert(num + " is even!");
	}else{
//		alert(num + " is odd");
	}
}

oddOrEvenChecker(56);


// AGE Checker 

function ageChecker(num){
	if(num < 17){
		alert(num + "is underaged");
		return false;
	}else{
		alert(num + "is allowed to drink!")
		return true;
	}
}

//let isAllowedToDrink = ageChecker(19);
//console.log(isAllowedToDrink);


//Truthy and Falsy Values

/*
	In JavaScript, a truthy value is a value that is considered true when encountered in a Boolean context.

	-Values are considered true unless defined otherwise
	-Falsy values/exceptions for truthy:
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN
*/

//Truthy Examples
/*
	if the result of an expression in a condition results to a truthy value, the condition returns true and the corresponding statements are executed
	-expressions are any unit of code that can be evaluated to a value
*/

if(true){
	console.log("true isTruthy");
}
if(1){
	console.log("1 is Truthy");
}
if([]){
	console.log("[] empty array is Truthy");
}
if("camille"){
	console.log("Camille is Truthy");
}

//Falsy Values

if(false){
	console.log("Falsy");
}
if(0){
	console.log("Falsy");
}
if(undefined){
	console.log("Falsy");
}else{
	console.log("undefined is falsy")
}


//Conditional Ternary Operator
//Ternary operator is used as a shorter alternative to if-else statements
// It is also able to implicitly return a value

//syntax:
//(condition) ? ifTrue : ifFalse;

let age = 17;
let result = age < 18? "Underage" : "LegalAge";
console.log(result);

/*
Let result = if(age<18){
	return "Underage";
}else {
	return "Legal age";
}

console.log(result);
*/



//Switch Statement
//the switch statement evaluates an expression and matches the expression's value to a case clause
//the switch will then execute the statements associated with that case, as well as statements in cases that follow the matching case

//.toLowerCase() function/method will change the input received from the prompt into all lowercase letters --- if the users use capitalized

//break terminates loop once a match has been found

//Syntax
/*
	switch (expression){
	
	case value:
		statement;
		break;
	default;
		statement;
		break;
	}

*/

let day = prompt ("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day){

	case "monday" : 
		console.log("The color of the day is red!");
		break;
	case "tuesday" : 
		console.log("The color of the day is orange!");
		break;
	case "wednesday" : 
		console.log("The color of the day is yellow!");
		break;
	case "thursday" : 
		console.log("The color of the day is green!");
		break;
	case "friday" : 
		console.log("The color of the day is violet!");
		break;
	case "saturday" : 
		console.log("The color of the day is indigo!");
		break;
	case "sunday" : 
		console.log("The color of the day is blue!");
		break;
	default:
		console.log("Please input a valid day");
		break;
}

//Mini Activity

function determineBear(bearNumber){
	let bear;
	switch (bearNumber){
	case 1 :
		alert("Hi I am Amy");
		break;
	case 2 :
		alert("Hi I am Lulu");
		break;
	case 3 :
		alert("Hi I am Morgan");
		break;
	default :
		bear = bearNumber + "is out of bounds.";
		break;
	}
	return bear;
}
determineBear(2);


// Try-Catch-Finally Statement

	//try catch statements are commonly used for error handling

	function showIntensityAlert(windspeed){
		try{

			alertat(determineTyphoonIntensity(windspeed));

		}catch(error){

			console.log(typeof error);
			console.warn(error.message);

		}finally{

			alert("Intensity updates will show new alert!")

		}
	}
	showIntensityAlert(56);


console.log("Hi");
//another example




